/********************************************************************************
 * Copyright 2009 The Robotics Group, The Maersk Mc-Kinney Moller Institute,
 * Faculty of Engineering, University of Southern Denmark
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ********************************************************************************/

#include <caros/esdcan_port.h>

#include <rw/common/TimerUtil.hpp>
#include <rw/core/macros.hpp>

#include <cstdio>
#include <vector>

using rwhw::ESDCANPort;

namespace
{

bool setBaud(NTCAN_HANDLE* handle, ESDCANPort::CanBaud baud)
{
    NTCAN_RESULT b = baud;
    int64_t ret       = canSetBaudrate(*handle, b);
    if (ret != NTCAN_SUCCESS)
    {
        RW_WARN("Cannot set baud to " << baud << " " << ret);
        return false;
    }
    return true;
}

void printFifoInfo(NTCAN_HANDLE* handle)
{
    int64_t lArg;
    NTCAN_RESULT status;
    status = canIoctl(*handle, NTCAN_IOCTL_GET_RX_MSG_COUNT, &lArg);
    std::cout << "Msgs in rx fifo queue: " << lArg << " " << status << std::endl;
}

bool openDevice(unsigned int netId, int64_t txQueueSize, int64_t rxQueueSize, NTCAN_HANDLE* handle)
{
    uint64_t mode = 0;
    int64_t txtimeout     = 100;
    int64_t rxtimeout     = 100;

    int64_t ret = canOpen(netId, mode, txQueueSize, rxQueueSize, txtimeout, rxtimeout, handle);

    if (ret != NTCAN_SUCCESS)
    {
        // std::cout << "Cannot open Net-Device " << netId << " " << ret << std::endl;
        canClose(*handle);
        return false;
    }

    return true;
}

bool getStatus(unsigned int netId, ESDCANPort::CanDeviceStatus* status)
{
    NTCAN_HANDLE h0;
    CAN_IF_STATUS cstat;

    // std::cout << "Tesing if can device exists: " << std::endl;

    if (!openDevice(netId, 1, 1, &h0))
        return false;

    int64_t ret;
    ret = canStatus(h0, &cstat);
    if (ret != NTCAN_SUCCESS)
    {
        // std::cout << "Cannot get Status of Net-Device "<< netId << std::endl;
        canClose(h0);
        return false;
    }

    uint32_t baudrate;
    ret = canGetBaudrate(h0, &baudrate);
    if (ret != NTCAN_SUCCESS)
    {
        // std::cout << "Cannot get Baudrate of Net-Device " << netId << std::endl;
        canClose(h0);
        return false;
    }

    printf("Net %3d: ID=%s "
           "Dll=%1X.%1X.%02X "
           "Driver=%1X.%1X.%02X"
           " Firmware=%1X.%1X.%02X\n"
           "         Hardware=%1X.%1X.%02X "
           "Baudrate=%08lx Status=%08lx Features=%04x\n",
           netId,
           cstat.boardid,
           cstat.dll >> 12,
           (cstat.dll >> 8) & 0xf,
           cstat.dll & 0xff,
           cstat.driver >> 12,
           (cstat.driver >> 8) & 0xf,
           cstat.driver & 0xff,
           cstat.firmware >> 12,
           (cstat.firmware >> 8) & 0xf,
           cstat.firmware & 0xff,
           cstat.hardware >> 12,
           (cstat.hardware >> 8) & 0xf,
           cstat.hardware & 0xff,
           static_cast<uint64_t>(baudrate),
           static_cast<uint64_t>(cstat.boardstatus),
           cstat.features);

    status->netid = netId;

    canClose(h0);
    return true;
}

}    // namespace

ESDCANPort::ESDCANPort(unsigned int netId, int64_t txQueueSize, int64_t rxQueueSize, CanBaud canBaud,
                       int transmitDelay) :
    _netId(netId),
    _txQueueSize(txQueueSize), _rxQueueSize(rxQueueSize), _canBaud(canBaud),
    _transmitDelay(transmitDelay), _portOpen(false), _handle(0)
{}

ESDCANPort::~ESDCANPort()
{
    if (_portOpen)
        close();
    // if( _instanceCnt == 0 )
    //   closeIEILibrary()
}

std::vector< ESDCANPort::CanDeviceStatus > ESDCANPort::getConnectedDevices()
{
    std::vector< CanDeviceStatus > canDevices;

    for (unsigned int i = 0; i < 255; i++)
    {
        CanDeviceStatus status;
        bool available = getStatus(i, &status);
        if (available)
        {
            canDevices.push_back(status);
        }
    }
    return canDevices;
}

ESDCANPort* ESDCANPort::getPortInstance(unsigned int netId, int64_t txQueueSize, int64_t rxQueueSize,
                                        CanBaud canBaud,
                                        int transmitDelay)    // To do: add baud ad can id type
{
    CanDeviceStatus status;
    bool available = getStatus(netId, &status);
    if (!available)
        return NULL;
    // To do: add the instance to a static list so that we don't risk instantiating it twice
    return new ESDCANPort(status.netid, txQueueSize, rxQueueSize, canBaud, transmitDelay);
}

bool ESDCANPort::isOpen()
{
    return _portOpen;
}

void ESDCANPort::setBaudRate(CanBaud canBaud)
{
    _canBaud = canBaud;
}

bool ESDCANPort::open()
{
    return open(0x00, 0xFF);
}

bool ESDCANPort::open(int idlow, int idhigh)
{
    _portOpen = false;

    if (!openDevice(_netId, _txQueueSize, _rxQueueSize, &_handle))
    {
        RW_WARN("Port cannot be openned!");
        return false;
    }

    /*HANDLE localHandle;

    if( !openDevice(_netId, _txQueueSize, _rxQueueSize, localHandle) ){
    RW_WARN("Port cannot be openned!");
    return false;
}

    std::cout << "handle is " << _handle << " local handle is " << localHandle << std::endl;
*/
    _portOpen = true;

    bool isBaudSet = false;
    while (!isBaudSet)
    {
        rw::common::TimerUtil::sleepMs(20);
        isBaudSet = setBaud(&_handle, _canBaud);
    }

    int64_t ret;

    // RW_WARN("\n\n\n\n!!! This is a special version if ESDCAN for the LWA that only listens on
    // ID's 163..169 !!!!\n\n\n\n\n");

    // int lwa_ack_can_id[7]={163,164,165,166,167,168,169};
    // int lwa_ack_can_id[7]={123,124,125,126,127,128,129}; //

    for (size_t i = static_cast<size_t>(idlow); i <= static_cast<size_t>(idhigh); i++)
    {
        ret = canIdAdd(_handle, i);
        if (ret != NTCAN_SUCCESS)
        {
            std::cout << "Cannot add id's! " << static_cast<uint16_t>(ret) << std::endl;
            continue;
        }
    }
    // To do: Following does not work as it should
    // enable recieving of following ids
    // long fromId = 0, toId = 1024;
    // ret = canIdRangeAdd(_handle, fromId, toId);

    uint32_t baud;
    ret = canGetBaudrate(_handle, &baud);
    if (ret != NTCAN_SUCCESS)
    {
        RW_WARN("Cannot read baudrate! " << static_cast<uint16_t>(ret));
        return false;
    }
    return true;
}

void ESDCANPort::close()
{
    _portOpen = false;
    int64_t ret  = canClose(_handle);
    if (ret != NTCAN_SUCCESS)
        RW_THROW("Error occured when closing can port!" << static_cast<uint16_t>(ret));
}

bool ESDCANPort::read(ESDCANPort::CanMessage* msg)
{
    int32_t nrOfMsg = 1;
    CMSG canMsgBuff[1];

    // read one message from rx-fifo using non-blocking call
    // long ret = canTake(_handle, canMsgBuff, &nrOfMsg);
    int64_t ret = canRead(_handle, &canMsgBuff[0], &nrOfMsg, NULL);

    // check if any error occurred
    if (ret != NTCAN_SUCCESS)
    {
        // RW_WARN("Error reading port! " << (unsigned short)ret);
        return false;
    }

    // check if any message was read

    if (nrOfMsg < 1)
    {
        return false;
    }

    // the message was read convert data to rw can msg type
    // msg.timeStamp = canMsgBuff[0].
    msg->id     = canMsgBuff[0].id;
    msg->length = canMsgBuff[0].len;
    for (size_t i = 0; i < msg->length; i++)
    {
        msg->data[i] = canMsgBuff[0].data[i];
    }
    return true;
}

bool ESDCANPort::write(unsigned int id, const std::vector< unsigned char >& raw_data)
{
    // Take a copy so that we can provide constness of the input.
    int32_t nrOfMsg = 1;
    CMSG msgBuff;
    msgBuff.id  = id;
    msgBuff.len = raw_data.size();
    for (size_t i = 0; i < raw_data.size(); i++)
    {
        msgBuff.data[i] = raw_data[i];
    }

    // long ret = canSend(_handle, &msgBuff, &nrOfMsg);
    int64_t ret = canWrite(_handle, &msgBuff, &nrOfMsg, NULL);
    rw::common::TimerUtil::sleepMs(_transmitDelay);

    // check if any error occurred
    if (ret != NTCAN_SUCCESS)
    {
        RW_WARN("Error writing: " << static_cast<uint16_t>(ret));
        return false;
    }

    // check if any message was written
    if (nrOfMsg < 1)
    {
        RW_WARN("No MSG was written");
        return false;
    }

    return true;
}
