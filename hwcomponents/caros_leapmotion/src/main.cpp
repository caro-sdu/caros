#include <Leap.h>
#include <caros/hand_detection/global.h>
#include <caros/leap_motion_node.h>
#include <ros/ros.h>
#include <iostream>
#include <vector>
#include <string>

std::vector<processParameter> parametersTable;
void initParameters()
{  //                                                    min| max| def
  parametersTable.push_back(processParameter("output", 0, 10, 2));
  parametersTable.push_back(processParameter("blend", 0, 100, 32));
  parametersTable.push_back(processParameter("sigma", 0, 20, 8));
  parametersTable.push_back(processParameter("median", 1, 49, 19));
  parametersTable.push_back(processParameter("threshold", 0, 255, 60));
  parametersTable.push_back(processParameter("search_multiplier", 0.1, 10, 3));
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "caros_demo_leapmotion");
  ros::NodeHandle nh("~");
  std::string name = nh.getNamespace();
  initParameters();
  caros::LeapMotionNode platform(nh, name);
  platform.start();

  return 0;
}
