#ifndef CAROS_HAND_DETECTION_GLOBAL_H
#define CAROS_HAND_DETECTION_GLOBAL_H

#define THETA_VECTOR 0
#define VICTORY_CORR 1
#define RNR_CORR 2

// #define SLIDER_STYLE "background-color: rgb(10, 10, 10);"
// #define SLIDER_LABEL_STYLE  "color: rgb(90, 160, 100); font: 10pt ""DejaVu Sans"";"
// #define FPS_LABEL_STYLE "color: rgb(80, 120, 180); font: 24pt ""DejaVu Sans""; \
//                          border-style: none; background: transparent;"
// #define FINGERS_INFO_STYLE "QTableView {color: rgb(90, 100, 130); font: 8pt ""DejaVu Sans Mono""; \
//                                         border-style: none; background: transparent;} \
//                             QHeaderView::section {color: rgb(80, 120, 180); font: 8pt ""DejaVu Sans Mono""; \
//                                                   border-style: none; background: transparent;} \
//                             QTableCornerButton::section {border-style: none; background: transparent;}"
// #define GESTURE_INFO_STYLE "color: rgb(90, 100, 130); font: 8pt ""DejaVu Sans Mono"";"

#include <string>
#include <vector>

struct processParameter
{
  std::string m_id_;
  int m_min_;
  int m_max_;
  int m_default_;

  processParameter()
  {}

  processParameter(std::string id, int min, int max, int _default) :
    m_id_(id), m_min_(min), m_max_(max), m_default_(_default)
  {}
};

extern std::vector<processParameter> parametersTable;

#endif  // CAROS_HAND_DETECTION_GLOBAL_H
