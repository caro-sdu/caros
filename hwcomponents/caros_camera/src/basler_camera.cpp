#include <caros/basler_camera.h>

#include <sensor_msgs/image_encodings.h>

#include <string>
#include <vector>
#include <algorithm>
#include <map>

namespace caros
{
BaslerCamera::BaslerCamera() : CameraInterface()
{
}

BaslerCamera::BaslerCamera(const std::string& serial) : CameraInterface(serial)
{
  gige_camera_initialized_ = false;
  Pylon::PylonInitialize();
  // Get the transport layer factory
  Pylon::CTlFactory& tl_factory = Pylon::CTlFactory::GetInstance();
  // Get all attached GigE cameras
  Pylon::DeviceInfoList_t devices;
  gige_cam_list_ = devices;
  nr_of_gige_cameras_ = tl_factory.EnumerateDevices(devices);
}

BaslerCamera::~BaslerCamera()
{
  Pylon::PylonTerminate();
}

bool BaslerCamera::init()
{
  Pylon::CTlFactory& tl_factory = Pylon::CTlFactory::GetInstance();
  Pylon::DeviceInfoList_t device_list;

  int dev_count = tl_factory.EnumerateDevices(device_list);
  if (dev_count == 0)
  {
    return false;
  }

  std::string user_defined_name_found;
  std::string serial_number_found;
  Pylon::CBaslerGigEDeviceInfo device_found;
  Pylon::DeviceInfoList_t::const_iterator it;
  bool found_desired_device = false;

  for (it = device_list.begin(); it != device_list.end(); ++it)
  {
    if ((it->GetUserDefinedName() == serial_.c_str()) || (it->GetSerialNumber() == serial_.c_str()))
    {
      // user_defined_name_found = it->GetUserDefinedName();
      serial_ = it->GetSerialNumber();
      found_desired_device = true;
      device_found = *it;
      break;
    }
  }

  if (!found_desired_device)
  {
    return false;
  }

  gige_camera_.Attach(Pylon::CTlFactory::GetInstance().CreateDevice(device_found));

  /* Setup parameters */
  GenApi::INodeMap& nodemap = setup(gige_camera_);

  if (gige_camera_.IsPylonDeviceAttached())
  {
    gige_camera_initialized_ = true;
    return true;
  }
  else
  {
    return false;
  }
}

void BaslerCamera::start()
{
  if (gige_camera_initialized_)
  {
    gige_camera_.StartGrabbing();
  }
}

std::string BaslerCamera::getSerial() const
{
  return gige_camera_.GetDeviceInfo().GetSerialNumber().c_str();
}

bool BaslerCamera::shutdown()
{
  gige_camera_.DestroyDevice();
  return true;
}

void BaslerCamera::stop()
{
  if (gige_camera_initialized_)
    gige_camera_.StopGrabbing();
}

bool BaslerCamera::isRunning() const
{
  if (gige_camera_initialized_)
    if (gige_camera_.IsGrabbing())
      return true;
    else
      return false;
  else
    return false;
}

std::string BaslerCamera::getName() const
{
  return gige_camera_.GetDeviceInfo().GetModelName().c_str();
}

bool BaslerCamera::getRawImage(sensor_msgs::ImagePtr& img, uint64_t& timestamp)
{
  // Wait for an image and then retrieve it, with timeout [ms]
  Pylon::CGrabResultPtr ptr_grab_result;
  gige_camera_.RetrieveResult(5000, ptr_grab_result, Pylon::TimeoutHandling_ThrowException);

  // Image grabbed successfully?
  if (ptr_grab_result->GrabSucceeded())
  {
    // Access the image data.
    const uint32_t rows = ptr_grab_result->GetHeight();
    const uint32_t cols = ptr_grab_result->GetWidth();

    const uint8_t* pImageBuffer = reinterpret_cast<uint8_t*>(ptr_grab_result->GetBuffer());

    int pixel_depth(0);
    try
    {
      // pylon PixelSize already contains the number of channels
      // the size is given in bit, wheras ROS provides it in byte
      pixel_depth = gige_camera_.PixelSize.GetIntValue() / 8;
    }
    catch (const GenICam::GenericException& e)
    {
      ROS_ERROR_STREAM("An exception while reading image pixel size occurred: " << e.GetDescription());
    }

    size_t img_size_byte = cols * rows * pixel_depth;

    timestamp = ptr_grab_result->GetTimeStamp();
    std_msgs::Header header;
    // img->header.stamp = ros::Time::now();
    img->encoding = cam_encoding_;  // Image encoding set through the dynamic reconfigurable parameter PixelFormat
    img->height = rows;
    img->width = cols;
    img->step = img->width * pixel_depth;
    img->data.assign(pImageBuffer, pImageBuffer + img_size_byte);

    return true;
  }
  else
  {
    ROS_ERROR_STREAM("Error: " << ptr_grab_result->GetErrorCode() << " " << ptr_grab_result->GetErrorDescription());
    return false;
  }
}

template <typename T>
std::string getParam(const GenApi::INodeMap& nodemap, const std::string& name)
{
  GenApi::CPointer<T> param(nodemap.GetNode(name.c_str()));
  return std::string(param->ToString().c_str());
}

template <>
std::string getParam<GenApi::IEnumeration>(const GenApi::INodeMap& nodemap, const std::string& name)
{
  GenApi::CPointer<GenApi::IEnumeration> param(nodemap.GetNode(name.c_str()));
  auto node = param->GetCurrentEntry();
  return std::string(node->GetSymbolic().c_str());
}

// Set any type of parameter
template <typename T>
bool setParam(GenApi::INodeMap& nodemap, const std::string& name, const std::string& value)
{
  GenApi::CPointer<T> param(nodemap.GetNode(name.c_str()));
  if (IsWritable(param))
  {
    param->FromString(value.c_str());
    ROS_DEBUG_STREAM("\t" << name << ": " << param->GetValue());
  }
  else
  {
    ROS_DEBUG_STREAM("Failed to set camera parameter '" << name << "' to '" << value << "'!");
    return false;
  }

  return true;
}

// Special case: IEnumeration, where we make an extra check for the existence of the enum value
template <>
bool setParam<GenApi::IEnumeration>(GenApi::INodeMap& nodemap, const std::string& name, const std::string& value)
{
  GenApi::CPointer<GenApi::IEnumeration> param(nodemap.GetNode(name.c_str()));
  if (GenApi::IsWritable(param) && GenApi::IsAvailable(param->GetEntryByName(value.c_str())))
  {  // Extra check here
    param->FromString(value.c_str());
    ROS_DEBUG_STREAM("\t" << name << ": " << param->GetCurrentEntry()->GetSymbolic());
  }
  else
  {
    ROS_DEBUG_STREAM("Failed to set camera parameter '" << name << "' to '" << value << "'!");
    return false;
  }

  return true;
}

GenApi::INodeMap& BaslerCamera::setup(Pylon::CInstantCamera& camera)
{
  ROS_DEBUG_STREAM("Setting parameters for: " << camera.GetDeviceInfo().GetModelName() << " ("
                                              << camera.GetDeviceInfo().GetSerialNumber() << ")");

  if (camera.IsOpen())
  {
    ROS_DEBUG("Camera is already open! Closing...");
    camera.Close();
  }

  camera.Open();

  GenApi::INodeMap& nodemap = camera.GetNodeMap();
  GenApi::INodeMap& tl_nodemap = camera.GetTLNodeMap();

  // Set the pixel format to Mono8
  //    setParam<IEnumeration>(nodemap, "PixelFormat", "Mono8");

  /*
   * Auto parameters for avoiding saturation
   *   - Set the AOI to the region where stuff actually is
   */
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIWidth", "400");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIHeight", "400");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIOffsetX", "400");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIOffsetY", "400");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIWidth", "600");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIHeight", "600");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIOffsetX", "400");
  //    setParam<IInteger>(nodemap, "AutoFunctionAOIOffsetY", "400");
  //    setParam<GenApi::IEnumeration>(nodemap, "GainAuto", "Continuous");
  //    setParam<GenApi::IEnumeration>(nodemap, "ExposureAuto", "Continuous");
  //    setParam<GenApi::IEnumeration>(nodemap, "BalanceWhiteAuto", "Continuous");
  //    setParam<IEnumeration>(nodemap, "GainAuto", "Once");
  //    setParam<IEnumeration>(nodemap, "ExposureAuto", "Once");
  //    setParam<IInteger>(nodemap, "AutoTargetValue", "50");
  //    setParam<IBoolean>(nodemap, "GammaEnable", "True");
  //    setParam<IFloat>(nodemap, "Gamma", "2");

  /*
   * Set heartbeat parameter
   */

  setParam<GenApi::IInteger>(tl_nodemap, "HeartbeatTimeout", std::to_string(3 * 1000));  // set heartbeat to 3 seconds.

  /*
   * Set some extra bandwidth parameters
   */
  // setParam<GenApi::IInteger>(nodemap, "GevSCPSPacketSize", "1500");  // MTU in bytes ( use jumbo frames)
  // setParam<GenApi::IInteger>(nodemap, "GevSCPD", "100");             // Inter-packet delay in ticks
  camera.MaxNumBuffer = 5;  // Avoid lags when running at < 30 Hz

  return nodemap;
}

// Adjust value to make it comply with range and increment passed.
//
// The parameter's minimum and maximum are always considered as valid values.
// If the increment is larger than one, the returned value will be: min + (n * inc).
// If the value doesn't meet these criteria, it will be rounded down to ensure compliance.
int64_t adjust(int64_t val, int64_t minimum, int64_t maximum, int64_t inc)
{
  // Check the input parameters.
  if (inc <= 0)
  {
    // Negative increments are invalid.
    throw LOGICAL_ERROR_EXCEPTION("Unexpected increment %d", inc);
  }
  if (minimum > maximum)
  {
    // Minimum must not be bigger than or equal to the maximum.
    throw LOGICAL_ERROR_EXCEPTION("minimum bigger than maximum.");
  }
  // Check the lower bound.
  if (val < minimum)
  {
    return minimum;
  }
  // Check the upper bound.
  if (val > maximum)
  {
    return maximum;
  }
  // Check the increment.
  if (inc == 1)
  {
    // Special case: all values are valid.
    return val;
  }
  else
  {
    // The value must be min + (n * inc).
    // Due to the integer division, the value will be rounded down.
    return minimum + (((val - minimum) / inc) * inc);
  }
}

bool BaslerCamera::setNewConfiguration(caros_camera::BaslerConfig& config, const uint32_t& level)
{
  GenApi::INodeMap& nodemap = gige_camera_.GetNodeMap();

  // Set the pixel format
  gige_camera_.StopGrabbing();
  setParam<GenApi::IEnumeration>(nodemap, "PixelFormat", config.PixelFormat);

  if (config.PixelFormat == "BayerBG8")
    cam_encoding_ = sensor_msgs::image_encodings::BAYER_BGGR8;
  else if (config.PixelFormat == "BayerRG8")
    cam_encoding_ = sensor_msgs::image_encodings::BAYER_RGGB8;
  else if (config.PixelFormat == "Mono8")
    cam_encoding_ = sensor_msgs::image_encodings::MONO8;
  else
    ROS_ERROR_STREAM("Unknown PixelFormat specified: " << config.PixelFormat);

  /*
   * Auto parameters for avoiding saturation
   */
  setParam<GenApi::IEnumeration>(nodemap, "GainAuto", config.GainAuto);
  setParam<GenApi::IEnumeration>(nodemap, "ExposureAuto", config.ExposureAuto);
  setParam<GenApi::IEnumeration>(nodemap, "BalanceWhiteAuto", config.BalanceWhiteAuto);
  //    setParam<IEnumeration>(nodemap, "GainAuto", "Once");
  //    setParam<IEnumeration>(nodemap, "ExposureAuto", "Once");
  //    setParam<IInteger>(nodemap, "AutoTargetValue", "50");
  //    setParam<IBoolean>(nodemap, "GammaEnable", "True");
  //    setParam<IFloat>(nodemap, "Gamma", "2");

  // Access the GainRaw integer type node. This node is available for Firewire and GigE Devices.
  GenApi::CIntegerPtr gain_raw(nodemap.GetNode("GainRaw"));
  if (gain_raw.IsValid() && config.GainAuto == "Off")
  {
    int64_t new_gain_raw = gain_raw->GetMin() + config.GainRaw;  // ((gain_raw->GetMax() - gain_raw->GetMin()) / 2)
    // Make sure the calculated value is valid.
    new_gain_raw = adjust(new_gain_raw, gain_raw->GetMin(), gain_raw->GetMax(), gain_raw->GetInc());
    gain_raw->SetValue(new_gain_raw);
    ROS_DEBUG_STREAM("Gain       : " << gain_raw->GetValue() << " (Min: " << gain_raw->GetMin()
                                     << "; Max: " << gain_raw->GetMax() << "; Inc: " << gain_raw->GetInc() << ")");
  }

  // Access the ExposureTimeRaw integer type node.
  GenApi::CIntegerPtr exposure_time_raw(nodemap.GetNode("ExposureTimeRaw"));
  if (exposure_time_raw.IsValid() && config.ExposureAuto == "Off")
  {
    int64_t new_exposure_time_raw = exposure_time_raw->GetMin() + config.ExposureTimeRaw;
    // Make sure the calculated value is valid.
    new_exposure_time_raw = adjust(new_exposure_time_raw, exposure_time_raw->GetMin(), exposure_time_raw->GetMax(),
                                   exposure_time_raw->GetInc());
    if (IsWritable(exposure_time_raw))
    {
      exposure_time_raw->SetValue(new_exposure_time_raw);
      ROS_DEBUG_STREAM("Exposure Time Raw       : " << gain_raw->GetValue() << " (Min: " << gain_raw->GetMin()
                                                    << "; Max: " << gain_raw->GetMax()
                                                    << "; Inc: " << gain_raw->GetInc() << ")");
    }
  }

  if (config.GammaEnable)
  {
    gige_camera_.GammaEnable.SetValue(true);

    double new_gamma = config.Gamma;
    if (new_gamma < gige_camera_.Gamma.GetMax() && new_gamma > gige_camera_.Gamma.GetMin())
      gige_camera_.Gamma.SetValue(new_gamma);
    else
      ROS_WARN_STREAM("Will not accept the specified gamma value"
                      << new_gamma << " the value must be between: " << gige_camera_.Gamma.GetMin() << " and "
                      << gige_camera_.Gamma.GetMax());
  }
  else
  {
    gige_camera_.GammaEnable.SetValue(false);
  }

  GenApi::CBooleanPtr acquisition_framerate_enable(nodemap.GetNode("AcquisitionFrameRateEnable"));

  if (config.AcquisitionFrameRateEnable)
  {
    acquisition_framerate_enable->SetValue(true);

    // Access the AcquisitionFrameRateAbs float type node.
    GenApi::CFloatPtr acquisition_framerate(nodemap.GetNode("AcquisitionFrameRateAbs"));
    if (acquisition_framerate.IsValid())
    {
      double new_acquisition_framerate = config.AcquisitionFrameRateAbs;
      if (new_acquisition_framerate < acquisition_framerate->GetMax() &&
          new_acquisition_framerate > acquisition_framerate->GetMin())
        acquisition_framerate->SetValue(new_acquisition_framerate);
      else
        ROS_WARN_STREAM("Will not accept the specified Acquisition Frame Rate"
                        << new_acquisition_framerate << " the value must be between: "
                        << acquisition_framerate->GetMin() << " and " << acquisition_framerate->GetMax());
    }
  }
  else
  {
    acquisition_framerate_enable->SetValue(false);
  }

  /*
   * Bandwidth parameters
   */
  setParam<GenApi::IInteger>(nodemap, "GevSCPSPacketSize", config.GevSCPSPacketSize);  // MTU in bytes
  if (getParam<GenApi::IInteger>(nodemap, "GevSCPSPacketSize") != config.GevSCPSPacketSize)
    ROS_ERROR("Failed to set packet size -  Expect errors during camera acquisition!");

  setParam<GenApi::IInteger>(nodemap, "GevSCPD", config.GevSCPD);  // Inter-packet delay in ticks
  if (getParam<GenApi::IInteger>(nodemap, "GevSCPD") != config.GevSCPD)
    ROS_ERROR("Failed to set inter-packet delay - Expect errors during camera acquisition!");

  gige_camera_.BalanceWhiteAuto.SetValue(Basler_GigECamera::BalanceWhiteAuto_Off);
  if (gige_camera_.BalanceWhiteAuto.GetValue() != Basler_GigECamera::BalanceWhiteAuto_Off)
    ROS_ERROR("Failed to disable white balance!");
  gige_camera_.ColorTransformationSelector.SetValue(Basler_GigECamera::ColorTransformationSelector_RGBtoRGB);
  if (gige_camera_.ColorTransformationSelector.GetValue() != Basler_GigECamera::ColorTransformationSelector_RGBtoRGB)
    ROS_ERROR("Failed to set color transformation!");
  gige_camera_.LightSourceSelector.SetValue(Basler_GigECamera::LightSourceSelector_Daylight);
  if (gige_camera_.LightSourceSelector.GetValue() != Basler_GigECamera::LightSourceSelector_Daylight)
    ROS_ERROR("Failed to set light source!");
  gige_camera_.BalanceWhiteReset.Execute();

  gige_camera_.ColorAdjustmentEnable.SetValue(config.ColorAdjustmentEnable);
  if (gige_camera_.ColorAdjustmentEnable.GetValue() != config.ColorAdjustmentEnable)
    ROS_ERROR("Disabling color adjustments failed!");

  gige_camera_.ColorTransformationMatrixFactorRaw.SetValue(config.ColorTransformationMatrixFactorRaw);
  if (gige_camera_.ColorTransformationMatrixFactorRaw.GetValue() != config.ColorTransformationMatrixFactorRaw)
    ROS_ERROR("Setting color transformation matrix factor failed!");

  gige_camera_.BalanceRatioSelector.SetValue(Basler_GigECamera::BalanceRatioSelector_Red);
  if (gige_camera_.BalanceRatioSelector.GetValue() != Basler_GigECamera::BalanceRatioSelector_Red)
    ROS_ERROR("Setting white balance color selector failed!");
  gige_camera_.BalanceRatioRaw.SetValue(config.ColorBalanceValueRed);
  if (gige_camera_.BalanceRatioRaw.GetValue() != config.ColorBalanceValueRed)
    ROS_ERROR("Setting red white balance color value failed!");

  gige_camera_.BalanceRatioSelector.SetValue(Basler_GigECamera::BalanceRatioSelector_Green);
  if (gige_camera_.BalanceRatioSelector.GetValue() != Basler_GigECamera::BalanceRatioSelector_Green)
    ROS_ERROR("Setting white balance color selector failed!");
  gige_camera_.BalanceRatioRaw.SetValue(config.ColorBalanceValueGreen);
  if (gige_camera_.BalanceRatioRaw.GetValue() != config.ColorBalanceValueGreen)
    ROS_ERROR("Setting green white balance color value failed!");

  gige_camera_.BalanceRatioSelector.SetValue(Basler_GigECamera::BalanceRatioSelector_Blue);
  if (gige_camera_.BalanceRatioSelector.GetValue() != Basler_GigECamera::BalanceRatioSelector_Blue)
    ROS_ERROR("Setting white balance color selector failed!");
  gige_camera_.BalanceRatioRaw.SetValue(config.ColorBalanceValueBlue);
  if (gige_camera_.BalanceRatioRaw.GetValue() != config.ColorBalanceValueBlue)
    ROS_ERROR("Setting blue white balance color value failed!");

  gige_camera_.StartGrabbing();

  return true;
}
}  // namespace caros
