#ifndef CAROS_CAMERA_INTERFACE_H
#define CAROS_CAMERA_INTERFACE_H

#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>

#include <string>
#include <map>
#include <memory>

#ifdef BUMBLEBEE_SUPPORTED
#include <caros_camera/PointGreyConfig.h>
#endif

#ifdef BASLER_SUPPORTED
#include <caros_camera/BaslerConfig.h>
#endif

#ifdef ENSENSO_SUPPORTED
#include <caros_camera/EnsensoConfig.h>
#endif

namespace caros
{
class CameraInterface
{
 public:
  // constructors
  CameraInterface();
  // fw constructor
  explicit CameraInterface(unsigned int serial);
  // gige constructor
  explicit CameraInterface(const std::string& serial);

  virtual ~CameraInterface();

  virtual bool init() = 0;

  virtual void start() = 0;

  virtual bool shutdown() = 0;

  virtual std::string getName() const = 0;

  virtual std::string getSerial() const = 0;

  virtual void stop()
  {
  }
  virtual bool getRawImage(sensor_msgs::ImagePtr& img, uint64_t& timestamp)
  {
    return false;
  }
  virtual bool getRawImagePair(sensor_msgs::ImagePtr& img_left, sensor_msgs::ImagePtr& img_right, uint64_t& timestamp)
  {
    return false;
  }
  virtual bool grabSingleCloud(sensor_msgs::PointCloud2Ptr& cloud)
  {
    return false;
  }
  virtual bool isRunning() const
  {
    return false;
  }
  virtual bool setupParameters(const std::map<std::string, bool>& bool_parameters,
                               const std::map<std::string, int>& int_parameters,
                               const std::map<std::string, double>& double_parameters)
  {
    return false;
  }

#ifdef ENSENSO_SUPPORTED
  virtual bool setNewConfiguration(caros_camera::EnsensoConfig& config, const uint32_t& level)
  {
    return false;
  }
#endif

#ifdef BASLER_SUPPORTED
  virtual bool setNewConfiguration(caros_camera::BaslerConfig& config, const uint32_t& level)
  {
    return false;
  }
#endif

#ifdef BUMBLEBEE_SUPPORTED
  virtual bool setNewConfiguration(caros_camera::PointGreyConfig& config, const uint32_t& level)
  {
    return false;
  }
#endif

  virtual bool getCameraInfo(const std::string& cam, sensor_msgs::CameraInfo& cam_info) const
  {
    return false;
  }

 protected:
  // GigE
  std::string serial_;
  // FireWire
  unsigned int fw_serial_;
};
}  // namespace caros

#endif  // CAROS_CAMERA_INTERFACE_H
