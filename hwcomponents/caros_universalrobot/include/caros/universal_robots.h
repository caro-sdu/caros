#ifndef CAROS_UNIVERSAL_ROBOTS_H
#define CAROS_UNIVERSAL_ROBOTS_H

#include <caros/ur_service_interface.h>
#include <caros/ur_action_interface.h>
#include <caros/caros_node_service_interface.h>
#include <caros/serial_device_service_interface.h>

#include <rw/core/math_fwd.hpp>

#include <memory>
#include <string>
#include <vector>

#define SUPPORTED_Q_LENGTH_FOR_UR 6
#define RUN_LOOP_FREQUENCY 500

namespace rw
{
namespace models
{
class WorkCell;
class Device;
}
}  // end namespaces

namespace rw
{
namespace invkin
{
class JacobianIKSolver;
}
}  // end namespaces

namespace ur_rtde
{
class RTDEControlInterface;
class RTDEIOInterface;
class RTDEReceiveInterface;
}    // namespace ur_rtde

namespace caros
{
class UniversalRobots : public caros::CarosNodeServiceInterface,
                        public caros::SerialDeviceServiceInterface,
                        public caros::URActionInterface,
                        public URServiceInterface
{
 public:
  explicit UniversalRobots(const ros::NodeHandle& nodehandle);

  virtual ~UniversalRobots();

  enum URNODE_ERRORCODE
  {
    URNODE_MISSING_PARAMETER = 1,
    URNODE_URSERVICE_CONFIGURE_FAIL,
    URNODE_SERIALDEVICESERVICE_CONFIGURE_FAIL,
    URNODE_COULD_NOT_INIT_UR_RTDE,
    URNODE_UNSUPPORTED_Q_LENGTH,
    URNODE_EXECUTION_TIMEOUT,
    URNODE_ROBOT_EMERGENCY_STOP,
    URNODE_ROBOT_PROTECTIVE_STOP,
    URNODE_INTERNAL_ERROR
  };

  enum SAFETY_MODES
  {
    SAFETY_MODE_NORMAL = 1,
    SAFETY_MODE_REDUCED = 2,
    SAFETY_MODE_PROTECTIVE_STOP = 3,
    SAFETY_MODE_SAFEGUARD_STOP = 5,
    SAFETY_MODE_SYSTEM_EMERGENCY_STOP = 6,
    SAFETY_MODE_ROBOT_EMERGENCY_STOP = 7,
    SAFETY_MODE_VIOLATION = 8,
    SAFETY_MODE_FAULT = 9
  };

  /************************************************************************
   * URServiceInterface and URActionInterface functions
   ************************************************************************/
  //! @copydoc URServiceInterface::servoQ
  bool urServoQ(const rw::math::Q& target, const float time, const float lookahead_time, const float gain);
  //! @copydoc URServiceInterface::servoStop
  bool urServoStop();
  //! @copydoc URServiceInterface::forceModeStart
  bool urForceModeStart(const rw::math::Transform3D<>& ref_t_offset, const rw::math::Q& selection,
                        const rw::math::Wrench6D<>& wrench_target, int type, const rw::math::Q& limits);
  //! @copydoc URServiceInterface::forceModeUpdate
  bool urForceModeUpdate(const rw::math::Wrench6D<>& wrench_target);
  //! @copydoc URServiceInterface::forceModeStop
  bool urForceModeStop();
  //! @copydoc URServiceInterface::urMoveLin
  bool urMoveLin(const rw::math::Transform3D<>& target, double speed, double acceleration);
  //! @copydoc URServiceInterface::urMovePtp
  bool urMovePtp(const rw::math::Q& target, double speed, double acceleration);
  //! @copydoc URServiceInterface::urMovePtpPath
  bool urMovePtpPath(const rw::trajectory::QPath& q_path);
  //! @copydoc URServiceInterface::urMoveVelQ
  bool urMoveVelQ(const rw::math::Q& q_vel, double acceleration, double time);
  //! @copydoc URServiceInterface::urMoveVelT
  bool urMoveVelT(const rw::math::VelocityScrew6D<>& t_vel, double acceleration, double time);
  //! @copydoc URServiceInterface::urMoveVelStop
  bool urMoveVelStop();
  //! @copydoc URServiceInterface::setPayload
  bool urSetPayload(const double& mass, const rw::math::Vector3D<>& com);
  //! @copydoc URServiceInterface::setIO
  bool urSetIO(const int& pin, const bool& value);
  //! @copydoc URServiceInterface::urMoveStop
  bool urMoveStop();
  //! @copydoc URServiceInterface::urGetRobotMode
  int urGetRobotMode();
  //! @copydoc URServiceInterface::sendCustomScriptFile
  bool urSendCustomScriptFile(const std::string& file_path);
  //! @copydoc URServiceInterface::sendCustomScriptFunction
  bool urSendCustomScriptFunction(const std::string& function_name, const std::string& script);
  //! @copydoc URActionInterface::zeroFtSensor
  bool urZeroFtSensor();

  /************************************************************************
   * SerialDeviceServiceInterface functions
   ************************************************************************/
  //! @copydoc caros::SerialDeviceServiceInterface::moveLin
  bool moveLin(const TransformAndSpeedAndAccelerationContainer_t& targets);
  //! @copydoc caros::SerialDeviceServiceInterface::movePtp
  bool movePtp(const QAndSpeedAndAccelerationContainer_t& targets);
  //! @copydoc caros::SerialDeviceServiceInterface::movePtpT
  bool movePtpT(const TransformAndSpeedAndBlendContainer_t& targets);
  //! @copydoc caros::SerialDeviceServiceInterface::moveVelQ
  bool moveVelQ(const rw::math::Q& q_vel);
  //! @copydoc caros::SerialDeviceServiceInterface::moveVelT
  bool moveVelT(const rw::math::VelocityScrew6D<>& t_vel);
  //! @copydoc caros::SerialDeviceServiceInterface::moveStop
  bool moveStop();

 protected:
  /************************************************************************
   * Hooks implemented from CarosNodeServiceInterface base class
   ************************************************************************/
  bool activateHook();
  bool recoverHook(const std::string& error_msg, const int64_t error_code);

  void runLoopHook();
  void errorLoopHook();
  void fatalErrorLoopHook();

 private:
  /* convenience functions */
  bool isInWorkingCondition();
  bool supportedQSize(const rw::math::Q& q);

 private:
  ros::NodeHandle nodehandle_;
  std::shared_ptr< ur_rtde::RTDEControlInterface > rtde_control_;
  std::shared_ptr< ur_rtde::RTDEIOInterface > rtde_io_;
  std::shared_ptr< ur_rtde::RTDEReceiveInterface > rtde_receive_;
  std::string device_ip_;
  bool use_serial_device_interface_;

  std::vector<double> fm_task_frame_;
  std::vector<int> fm_selection_;
  int fm_type_;
  std::vector<double> fm_limits_;
};
}  // namespace caros
#endif  // CAROS_UNIVERSAL_ROBOTS_H
